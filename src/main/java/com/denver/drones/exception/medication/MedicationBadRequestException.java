package com.denver.drones.exception.medication;

import lombok.Getter;
import lombok.ToString;
import org.springframework.http.HttpStatus;

@Getter
@ToString
public class MedicationBadRequestException extends RuntimeException {

    private final HttpStatus status;

    public MedicationBadRequestException(HttpStatus status) {
        super();
        this.status = status;
    }
}

package com.denver.drones.exception.medication;

import com.denver.drones.utils.record.ApiError;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@Slf4j
@ControllerAdvice
public class MedicationExceptionHandlerAdvice {

    @ResponseStatus(
            value = HttpStatus.BAD_REQUEST,
            reason = "Requested medication not found"
    )
    @ExceptionHandler(MedicationNotFoundException.class)
    public ResponseEntity<ApiError> handleException(MedicationNotFoundException e) {
        ApiError error = new ApiError(HttpStatus.BAD_REQUEST, e.getLocalizedMessage());
        log.debug("Medication not found error: {}", e.getLocalizedMessage());
        return new ResponseEntity<>(error, error.status());
    }

    @ResponseStatus(
            value = HttpStatus.BAD_REQUEST,
            reason = "Input data is not correct"
    )
    @ExceptionHandler(MedicationBadRequestException.class)
    public ResponseEntity<ApiError> handleException(MedicationBadRequestException e) {
        ApiError error = new ApiError(HttpStatus.BAD_REQUEST, e.getLocalizedMessage());
        log.debug("Input error error: {}", e.getLocalizedMessage());
        return new ResponseEntity<>(error, error.status());
    }
}

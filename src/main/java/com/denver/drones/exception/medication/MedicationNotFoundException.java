package com.denver.drones.exception.medication;

import lombok.Getter;
import lombok.ToString;
import org.springframework.http.HttpStatus;

@Getter
@ToString
public class MedicationNotFoundException extends RuntimeException {

    private final HttpStatus status;

    public MedicationNotFoundException(HttpStatus status) {
        super();
        this.status = status;
    }
}

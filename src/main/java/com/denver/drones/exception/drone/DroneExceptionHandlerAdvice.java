package com.denver.drones.exception.drone;

import com.denver.drones.utils.record.ApiError;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@Slf4j
@ControllerAdvice
public class DroneExceptionHandlerAdvice {

    @ResponseStatus(
            value = HttpStatus.BAD_REQUEST,
            reason = "The maximum allowed number of drones has been exceeded"
    )
    @ExceptionHandler(DroneMaxAmountException.class)
    public ResponseEntity<ApiError> handleException(DroneMaxAmountException e) {
        ApiError error = new ApiError(HttpStatus.BAD_REQUEST, e.getLocalizedMessage());
        log.debug("Creating a new drone error: {}", e.getLocalizedMessage());
        return new ResponseEntity<>(error, error.status());
    }

    @ResponseStatus(
            value = HttpStatus.BAD_REQUEST,
            reason = "Input data is not correct"
    )
    @ExceptionHandler(DroneBadRequestException.class)
    public ResponseEntity<ApiError> handleException(DroneBadRequestException e) {
        ApiError error = new ApiError(HttpStatus.BAD_REQUEST, e.getLocalizedMessage());
        log.debug("Input error: {}", e.getLocalizedMessage());
        return new ResponseEntity<>(error, error.status());
    }

    @ResponseStatus(
            value = HttpStatus.BAD_REQUEST,
            reason = "Requested drone not found"
    )
    @ExceptionHandler(DroneNotFoundException.class)
    public ResponseEntity<ApiError> handleException(DroneNotFoundException e) {
        ApiError error = new ApiError(HttpStatus.BAD_REQUEST, e.getLocalizedMessage());
        log.debug("Drone not found in database error: {}", e.getLocalizedMessage());
        return new ResponseEntity<>(error, error.status());
    }

    @ResponseStatus(
            value = HttpStatus.BAD_REQUEST,
            reason = "The weight of medicines exceeds the allowed weight of the drone"
    )
    @ExceptionHandler(DroneOverweightBadRequestException.class)
    public ResponseEntity<ApiError> handleException(DroneOverweightBadRequestException e) {
        ApiError error = new ApiError(HttpStatus.BAD_REQUEST, e.getLocalizedMessage());
        log.debug("Drone overweight error: {}", e.getLocalizedMessage());
        return new ResponseEntity<>(error, error.status());
    }

    @ResponseStatus(
            value = HttpStatus.BAD_REQUEST,
            reason = "This drone does a different job"
    )
    @ExceptionHandler(DroneIsBusyException.class)
    public ResponseEntity<ApiError> handleException(DroneIsBusyException e) {
        ApiError error = new ApiError(HttpStatus.BAD_REQUEST, e.getLocalizedMessage());
        log.debug("Busy drone error: {}", e.getLocalizedMessage());
        return new ResponseEntity<>(error, error.status());
    }

    @ResponseStatus(
            value = HttpStatus.BAD_REQUEST,
            reason = "Drone has low battery level"
    )
    @ExceptionHandler(DroneBatteryLevelIsLowException.class)
    public ResponseEntity<ApiError> handleException(DroneBatteryLevelIsLowException e) {
        ApiError error = new ApiError(HttpStatus.BAD_REQUEST, e.getLocalizedMessage());
        log.debug("Busy drone error: {}", e.getLocalizedMessage());
        return new ResponseEntity<>(error, error.status());
    }
}

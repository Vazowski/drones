package com.denver.drones.exception.drone;

import lombok.Getter;
import lombok.ToString;
import org.springframework.http.HttpStatus;

@Getter
@ToString
public class DroneIsBusyException extends RuntimeException {

    private final HttpStatus status;

    public DroneIsBusyException(HttpStatus status) {
        super();
        this.status = status;
    }
}

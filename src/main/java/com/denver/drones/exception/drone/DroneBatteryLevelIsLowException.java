package com.denver.drones.exception.drone;

import lombok.Getter;
import lombok.ToString;
import org.springframework.http.HttpStatus;

@Getter
@ToString
public class DroneBatteryLevelIsLowException extends RuntimeException {

    private final HttpStatus status;

    public DroneBatteryLevelIsLowException(HttpStatus status) {
        super();
        this.status = status;
    }
}

package com.denver.drones.exception.drone;

import lombok.Getter;
import lombok.ToString;
import org.springframework.http.HttpStatus;

@Getter
@ToString
public class DroneMaxAmountException extends RuntimeException {

    private final HttpStatus status;

    public DroneMaxAmountException(HttpStatus status) {
        super();
        this.status = status;
    }
}

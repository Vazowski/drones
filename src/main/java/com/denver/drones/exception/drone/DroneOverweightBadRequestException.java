package com.denver.drones.exception.drone;

import lombok.Getter;
import lombok.ToString;
import org.springframework.http.HttpStatus;

@Getter
@ToString
public class DroneOverweightBadRequestException extends RuntimeException {

    private final HttpStatus status;

    public DroneOverweightBadRequestException(HttpStatus status) {
        super();
        this.status = status;
    }
}

package com.denver.drones.controller;

import com.denver.drones.model.medication.request.MedicationCreateRequest;
import com.denver.drones.model.medication.response.MedicationCreateResponse;
import com.denver.drones.model.medication.response.MedicationDeleteResponse;
import com.denver.drones.model.medication.response.MedicationInfoResponse;
import com.denver.drones.service.MedicationService;
import jakarta.validation.Valid;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for medication endpoints
 */
@RestController
@RequestMapping("/api/v1/medication")
public class MedicationController {


    private final MedicationService medicationService;

    public MedicationController(MedicationService medicationService) {
        this.medicationService = medicationService;
    }

    @PostMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MedicationCreateResponse> medicationCreate(@Valid @RequestBody MedicationCreateRequest request) {
        return medicationService.createMedication(request);
    }

    @PostMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MedicationCreateResponse> medicationUpdate(@PathVariable Long id,
                                                                     @Valid @RequestBody MedicationCreateRequest request) {
        return medicationService.updateMedication(id, request);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MedicationInfoResponse> medicationInfo(@PathVariable Long id) {
        return medicationService.getMedicationInfo(id);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MedicationDeleteResponse> medicationDelete(@PathVariable Long id) {
        return medicationService.deleteMedication(id);
    }
}

package com.denver.drones.controller;

import com.denver.drones.constants.DroneState;
import com.denver.drones.model.drone.reponse.DroneCreateResponse;
import com.denver.drones.model.drone.reponse.DroneDeleteResponse;
import com.denver.drones.model.drone.reponse.DroneInfoResponse;
import com.denver.drones.model.drone.reponse.DroneLoadResponse;
import com.denver.drones.model.drone.reponse.DronesAvailableResponse;
import com.denver.drones.model.drone.reponse.DroneBatteryResponse;
import com.denver.drones.model.drone.request.DroneCreateRequest;
import com.denver.drones.model.drone.request.DroneLoadRequest;
import com.denver.drones.service.DroneService;
import jakarta.validation.Valid;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for drone endpoints
 */
@RestController
@RequestMapping("/api/v1/drone")
public class DroneController {

    private final DroneService droneService;

    public DroneController(DroneService droneService) {
        this.droneService = droneService;
    }

    @PostMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DroneCreateResponse> droneCreate(@Valid @RequestBody DroneCreateRequest request) {
        return droneService.createDrone(request);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DroneInfoResponse> droneInfo(@PathVariable Long id) {
        return droneService.getDroneInfo(id);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DroneDeleteResponse> droneDelete(@PathVariable Long id) {
        return droneService.deleteDrone(id);
    }

    @PutMapping(value = "/load/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DroneLoadResponse> droneLoad(@PathVariable Long id,
                                                       @RequestBody DroneLoadRequest request) {
        return droneService.loadDrone(id, request);
    }

    @GetMapping(value = "/available", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DronesAvailableResponse> dronesAvailable() {
        return droneService.listOfDronesByState(DroneState.IDLE);
    }

    @GetMapping(value = "/battery/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DroneBatteryResponse> droneBattery(@PathVariable Long id) {
        return droneService.droneWithBatteryInfo(id);
    }
}

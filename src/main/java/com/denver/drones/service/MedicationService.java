package com.denver.drones.service;

import com.denver.drones.entity.Medication;
import com.denver.drones.model.medication.request.MedicationCreateRequest;
import com.denver.drones.model.medication.response.MedicationCreateResponse;
import com.denver.drones.model.medication.response.MedicationDeleteResponse;
import com.denver.drones.model.medication.response.MedicationInfoResponse;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface MedicationService {

    ResponseEntity<MedicationCreateResponse> createMedication(MedicationCreateRequest request);
    ResponseEntity<MedicationCreateResponse> updateMedication(Long id, MedicationCreateRequest request);
    ResponseEntity<MedicationInfoResponse> getMedicationInfo(Long id);
    ResponseEntity<MedicationDeleteResponse> deleteMedication(Long id);
    List<Medication> findAll();
}

package com.denver.drones.service;

import com.denver.drones.constants.DroneState;
import com.denver.drones.entity.Drone;
import com.denver.drones.model.drone.reponse.*;
import com.denver.drones.model.drone.request.DroneCreateRequest;
import com.denver.drones.model.drone.request.DroneLoadRequest;
import org.springframework.http.ResponseEntity;

public interface DroneService {

    ResponseEntity<DroneCreateResponse> createDrone(DroneCreateRequest request);

    ResponseEntity<DroneInfoResponse> getDroneInfo(Long id);

    ResponseEntity<DroneDeleteResponse> deleteDrone(Long id);

    ResponseEntity<DroneLoadResponse> loadDrone(Long id, DroneLoadRequest request);

    ResponseEntity<DronesAvailableResponse> listOfDronesByState(DroneState state);

    ResponseEntity<DroneBatteryResponse> droneWithBatteryInfo(Long id);
    void setDroneBattery(Long id, int batteryCapacityChangeValue);
    void updateDroneState(Drone drone, DroneState state);
}



package com.denver.drones.service.impl;

import com.denver.drones.config.DronesConfiguration;
import com.denver.drones.constants.DroneState;
import com.denver.drones.entity.Drone;
import com.denver.drones.entity.Medication;
import com.denver.drones.exception.drone.DroneBadRequestException;
import com.denver.drones.exception.drone.DroneBatteryLevelIsLowException;
import com.denver.drones.exception.drone.DroneIsBusyException;
import com.denver.drones.exception.drone.DroneMaxAmountException;
import com.denver.drones.exception.drone.DroneNotFoundException;
import com.denver.drones.model.drone.reponse.DroneBatteryResponse;
import com.denver.drones.model.drone.reponse.DroneCreateResponse;
import com.denver.drones.model.drone.reponse.DroneDeleteResponse;
import com.denver.drones.model.drone.reponse.DroneInfoResponse;
import com.denver.drones.model.drone.reponse.DroneLoadResponse;
import com.denver.drones.model.drone.reponse.DronesAvailableResponse;
import com.denver.drones.model.drone.request.DroneCreateRequest;
import com.denver.drones.model.drone.request.DroneLoadRequest;
import com.denver.drones.repository.DroneRepository;
import com.denver.drones.service.DroneService;
import com.denver.drones.service.MedicationService;
import com.denver.drones.utils.DroneUtils;
import com.denver.drones.utils.record.DroneIdWithBattery;
import com.denver.drones.utils.record.DroneIdWithType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Service
public class DroneServiceImpl implements DroneService {

    private static final int MINIMUM_ALLOWABLE_DRONE_CHARGE_LEVEL = 25;

    private final Long maxDronesAmount;
    private final List<String> droneTypes;

    private final DroneRepository droneRepository;
    private final MedicationService medicationService;

    private Long currentDronesAmount;

    public DroneServiceImpl(DroneRepository droneRepository,
                            MedicationService medicationService,
                            DronesConfiguration dronesConfiguration) {
        this.droneRepository = droneRepository;
        this.medicationService = medicationService;
        this.maxDronesAmount = dronesConfiguration.getMaxDronesAmount();
        this.droneTypes = dronesConfiguration.getDroneTypes();

        currentDronesAmount = droneRepository.count();
    }

    @Override
    public ResponseEntity<DroneCreateResponse> createDrone(DroneCreateRequest request) {
        log.trace("Create drone attempt. Request: {}", request);
        currentDronesAmount = droneRepository.count();

        if (!droneTypes.contains(request.getDroneModel()) || request.getSerialNumber().length() > 100) {
            throw new DroneBadRequestException(HttpStatus.BAD_REQUEST);
        }

        if (maxDronesAmount.equals(currentDronesAmount)) {
            throw new DroneMaxAmountException(HttpStatus.BAD_REQUEST);
        }

        droneRepository.save(new Drone(request.getSerialNumber(), request.getDroneModel()));
        currentDronesAmount++;
        log.trace("Drone successfully created. CurrentDronesAmount: {}", currentDronesAmount);

        return ResponseEntity.ok(new DroneCreateResponse(HttpStatus.OK, "Drone successfully created"));
    }

    @Override
    public ResponseEntity<DroneInfoResponse> getDroneInfo(Long id) {
        return droneRepository.findById(id)
                .map(drone -> {
                    log.trace("Drone successfully found in the database. Drone: {}", drone);
                    return ResponseEntity.ok(new DroneInfoResponse(HttpStatus.OK, drone));
                })
                .orElseThrow(() -> new DroneNotFoundException(HttpStatus.BAD_REQUEST));
    }

    @Override
    public ResponseEntity<DroneDeleteResponse> deleteDrone(Long id) {
        if (droneRepository.existsById(id)) {
            droneRepository.deleteById(id);
        } else {
            throw new DroneNotFoundException(HttpStatus.BAD_REQUEST);
        }
        currentDronesAmount--;
        log.trace("Drone with id - {} successfully deleted. CurrentDronesAmount: {}", id, currentDronesAmount);

        return ResponseEntity.ok(new DroneDeleteResponse(HttpStatus.OK, "Drone successfully deleted"));
    }

    @Override
    public ResponseEntity<DroneLoadResponse> loadDrone(Long id, DroneLoadRequest request) {
        Optional<Drone> droneOptional = droneRepository.findById(id);
        if (droneOptional.isEmpty()) {
            throw new DroneNotFoundException(HttpStatus.BAD_REQUEST);
        }

        Drone drone = droneOptional.get();
        if (DroneState.DELIVERED.name().equals(drone.getDroneState())) {
            return ResponseEntity.ok(new DroneLoadResponse(HttpStatus.OK, "Drone returning after delivery"));
        }

        if (!DroneState.IDLE.name().equals(drone.getDroneState())) {
            throw new DroneIsBusyException(HttpStatus.BAD_REQUEST);
        }

        if (drone.getBatteryCapacity() <= MINIMUM_ALLOWABLE_DRONE_CHARGE_LEVEL) {
            throw new DroneBatteryLevelIsLowException(HttpStatus.BAD_REQUEST);
        }

        List<Medication> medications = medicationService.findAll();
        List<String> requestMedication = request.getMedication();
        List<String> droneTargetMedications = DroneUtils.checkDroneCanCarryMedications(drone, medications, requestMedication);

        drone.setMedication(String.join(", ", droneTargetMedications));
        drone.setDroneState(DroneState.LOADED.name());
        droneRepository.save(drone);
        log.trace("Drone: {} successfully loaded with medicines: {}", drone, request.getMedication());

        return ResponseEntity.ok(new DroneLoadResponse(HttpStatus.OK, "Drone successfully loaded with medicines"));
    }

    @Override
    public ResponseEntity<DronesAvailableResponse> listOfDronesByState(DroneState state) {
        List<Drone> drones = droneRepository.findAll();
        List<DroneIdWithType> result = drones.stream()
                .filter(drone -> state.name().equals(drone.getDroneState()))
                .map(drone -> new DroneIdWithType(drone.getId(), drone.getDroneModel()))
                .toList();

        return ResponseEntity.ok().body(new DronesAvailableResponse(HttpStatus.OK, result));
    }

    @Override
    public ResponseEntity<DroneBatteryResponse> droneWithBatteryInfo(Long id) {
        List<Drone> drones = droneRepository.findAll();
        Optional<DroneIdWithBattery> result = drones.stream()
                .filter(drone -> Objects.equals(drone.getId(), id))
                .map(drone -> new DroneIdWithBattery(drone.getId(), drone.getBatteryCapacity()))
                .findFirst();

        if (result.isPresent()) {
            return ResponseEntity.ok().body(new DroneBatteryResponse(HttpStatus.OK, result.get()));
        }

        throw new DroneNotFoundException(HttpStatus.BAD_REQUEST);
    }

    @Override
    public void updateDroneState(Drone drone, DroneState state) {
        String initialDroneState = drone.getDroneState();
        drone.setDroneState(state.name());
        droneRepository.save(drone);
        if (!initialDroneState.equals(state.name())) {
            log.debug("Drone#{} state successfully updated from {} to {}", drone.getId(), initialDroneState, state);
        }
    }

    @Override
    public void setDroneBattery(Long id, int batteryCapacityChangeValue) {
        Optional<Drone> droneOptional = droneRepository.findById(id);
        if (droneOptional.isEmpty()) {
            throw new DroneNotFoundException(HttpStatus.BAD_REQUEST);
        }

        Drone drone = droneOptional.get();
        int initialDroneBatteryValue = drone.getBatteryCapacity();
        int droneCurrentBatteryValue = initialDroneBatteryValue;

        if (batteryCapacityChangeValue < 0) {
            droneCurrentBatteryValue = Math.max(droneCurrentBatteryValue - Math.abs(batteryCapacityChangeValue), 0);
        } else {
            droneCurrentBatteryValue = Math.min(droneCurrentBatteryValue + batteryCapacityChangeValue, 100);
        }

        drone.setBatteryCapacity(droneCurrentBatteryValue);
        droneRepository.save(drone);
        log.debug("Drone#{} battery successfully updated from {}% to {}%", drone.getId(), initialDroneBatteryValue, droneCurrentBatteryValue);
    }
}

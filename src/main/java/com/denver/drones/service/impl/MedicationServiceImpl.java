package com.denver.drones.service.impl;

import com.denver.drones.entity.Medication;
import com.denver.drones.exception.medication.MedicationBadRequestException;
import com.denver.drones.exception.medication.MedicationNotFoundException;
import com.denver.drones.model.medication.request.MedicationCreateRequest;
import com.denver.drones.model.medication.response.MedicationCreateResponse;
import com.denver.drones.model.medication.response.MedicationDeleteResponse;
import com.denver.drones.model.medication.response.MedicationInfoResponse;
import com.denver.drones.repository.MedicationRepository;
import com.denver.drones.service.MedicationService;
import com.denver.drones.utils.Validator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class MedicationServiceImpl implements MedicationService {

    private final MedicationRepository medicationRepository;

    public MedicationServiceImpl(MedicationRepository medicationRepository) {
        this.medicationRepository = medicationRepository;
    }

    @Override
    public ResponseEntity<MedicationCreateResponse> createMedication(MedicationCreateRequest request) {
        log.trace("Create medication attempt. Request: {}", request);
        Medication medication = new Medication(
                request.getName(),
                request.getWeight(),
                request.getCode(),
                request.getImage()
        );

        if (Validator.isMedicationDataWrong(medication)) {
            throw new MedicationBadRequestException(HttpStatus.BAD_REQUEST);
        }

        medicationRepository.save(medication);

        return ResponseEntity.ok(new MedicationCreateResponse(HttpStatus.OK, "Medication successfully created"));
    }

    @Override
    public ResponseEntity<MedicationCreateResponse> updateMedication(Long id, MedicationCreateRequest request) {
        log.trace("Update medication attempt. Request: {}", request);
        Optional<Medication> medicationOptional = medicationRepository.findById(id);
        if (medicationOptional.isEmpty()) {
            throw new MedicationNotFoundException(HttpStatus.BAD_REQUEST);
        }

        Medication medication = medicationOptional.get();
        medication.setName(request.getName());
        medication.setWeight(request.getWeight());
        medication.setCode(request.getCode());
        medication.setImage(request.getImage());

        if (Validator.isMedicationDataWrong(medication)) {
            throw new MedicationBadRequestException(HttpStatus.BAD_REQUEST);
        }

        medicationRepository.save(medication);

        return ResponseEntity.ok(new MedicationCreateResponse(HttpStatus.OK, "Medication successfully updated"));
    }

    @Override
    public ResponseEntity<MedicationInfoResponse> getMedicationInfo(Long id) {
        return medicationRepository.findById(id)
                .map(medication -> {
                    log.trace("Medication successfully found in the database. Medication: {}", medication);
                    return ResponseEntity.ok(new MedicationInfoResponse(HttpStatus.OK, medication));
                })
                .orElseThrow(() -> new MedicationNotFoundException(HttpStatus.BAD_REQUEST));
    }

    @Override
    public ResponseEntity<MedicationDeleteResponse> deleteMedication(Long id) {
        if (medicationRepository.existsById(id)) {
            medicationRepository.deleteById(id);
        } else {
            throw new MedicationNotFoundException(HttpStatus.BAD_REQUEST);
        }

        log.trace("Medication with id - {} successfully deleted", id);
        return ResponseEntity.ok(new MedicationDeleteResponse(HttpStatus.OK, "Medication successfully deleted"));
    }

    @Override
    public List<Medication> findAll() {
        return medicationRepository.findAll();
    }
}

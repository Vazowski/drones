package com.denver.drones;

import com.denver.drones.config.DronesConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@Slf4j
@SpringBootApplication
@EnableConfigurationProperties(DronesConfiguration.class)
public class DronesApp {

	public static void main(String[] args) {
		try{
			SpringApplication.run(DronesApp.class, args);
		} catch (Exception e) {
			log.error("Application run error: {}", e.getMessage());
		}
	}
}
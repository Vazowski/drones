package com.denver.drones.model.drone.reponse;

import com.denver.drones.model.Response;
import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.time.Instant;

@Getter
public class DroneCreateResponse extends Response {

    private final String message;

    public DroneCreateResponse(HttpStatus status, String message) {
        super(status, Instant.now());
        this.message = message;
    }
}

package com.denver.drones.model.drone.reponse;

import com.denver.drones.model.Response;
import com.denver.drones.utils.record.DroneIdWithBattery;
import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.time.Instant;

@Getter
public class DroneBatteryResponse extends Response {

    private final DroneIdWithBattery drone;

    public DroneBatteryResponse(HttpStatus status, DroneIdWithBattery drone) {
        super(status, Instant.now());
        this.drone = drone;
    }
}

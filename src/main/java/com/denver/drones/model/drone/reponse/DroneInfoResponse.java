package com.denver.drones.model.drone.reponse;

import com.denver.drones.entity.Drone;
import com.denver.drones.model.Response;
import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.time.Instant;

@Getter
public class DroneInfoResponse extends Response {

    private final Drone drone;

    public DroneInfoResponse(HttpStatus status, Drone drone) {
        super(status, Instant.now());
        this.drone = drone;
    }
}

package com.denver.drones.model.drone.reponse;

import com.denver.drones.model.Response;
import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.time.Instant;

@Getter
public class DroneLoadResponse extends Response {

    private final String message;

    public DroneLoadResponse(HttpStatus status, String message) {
        super(status, Instant.now());
        this.message = message;
    }
}

package com.denver.drones.model.drone.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class DroneCreateRequest {

    private String serialNumber;
    private String droneModel;

    @Override
    public String toString() {
        return "DroneCreateRequest{" +
                "serialNumber='" + serialNumber + '\'' +
                ", droneModel='" + droneModel + '\'' +
                '}';
    }
}

package com.denver.drones.model.drone.reponse;

import com.denver.drones.utils.record.DroneIdWithType;
import com.denver.drones.model.Response;
import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.time.Instant;
import java.util.List;

@Getter
public class DronesAvailableResponse extends Response {

    private final List<DroneIdWithType> droneList;

    public DronesAvailableResponse(HttpStatus status, List<DroneIdWithType> droneList) {
        super(status, Instant.now());
        this.droneList = droneList;
    }
}

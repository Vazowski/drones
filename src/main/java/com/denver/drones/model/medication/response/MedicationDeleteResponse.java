package com.denver.drones.model.medication.response;

import com.denver.drones.model.Response;
import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.time.Instant;

@Getter
public class MedicationDeleteResponse extends Response {

    private final String message;

    public MedicationDeleteResponse(HttpStatus status, String message) {
        super(status, Instant.now());
        this.message = message;
    }
}

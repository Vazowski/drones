package com.denver.drones.model.medication.response;

import com.denver.drones.entity.Medication;
import com.denver.drones.model.Response;
import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.time.Instant;

@Getter
public class MedicationInfoResponse extends Response {

    private final Medication medication;

    public MedicationInfoResponse(HttpStatus status, Medication medication) {
        super(status, Instant.now());
        this.medication = medication;
    }
}

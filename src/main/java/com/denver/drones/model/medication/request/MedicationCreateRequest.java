package com.denver.drones.model.medication.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class MedicationCreateRequest {

    private String name;
    private int weight;
    private String code;
    private String image;

}

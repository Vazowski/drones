package com.denver.drones.model;

import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.time.Instant;

@Getter
public abstract class Response {

    private final HttpStatus status;
    private final Instant actualTimeStamp;

    protected Response(HttpStatus status, Instant actualTimeStamp) {
        this.status = status;
        this.actualTimeStamp = actualTimeStamp;
    }
}

package com.denver.drones.utils;

import com.denver.drones.constants.DroneState;
import com.denver.drones.entity.Drone;
import com.denver.drones.entity.Medication;
import com.denver.drones.exception.drone.DroneBatteryLevelIsLowException;
import com.denver.drones.exception.drone.DroneIsBusyException;
import com.denver.drones.exception.drone.DroneOverweightBadRequestException;
import com.denver.drones.model.drone.reponse.DroneInfoResponse;
import com.denver.drones.model.drone.reponse.DronesAvailableResponse;
import com.denver.drones.model.drone.request.DroneLoadRequest;
import com.denver.drones.service.DroneService;
import com.denver.drones.service.MedicationService;
import com.denver.drones.utils.record.DroneIdWithType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Stream;

@Slf4j
@Service
public class DemoScheduler {

    private final DroneService droneService;
    private final MedicationService medicationService;

    public DemoScheduler(DroneService droneService, MedicationService medicationService) {
        this.droneService = droneService;
        this.medicationService = medicationService;
        log.info("Scheduler started");
    }

    @Scheduled(fixedDelay = 9000)
    private void scheduleDroneLoad() {
        DronesAvailableResponse response = droneService.listOfDronesByState(DroneState.IDLE).getBody();
        List<DroneIdWithType> availableDrones = checkAvailableDrones(response);

        if (!availableDrones.isEmpty()) {
            try {
                List<Medication> medications = medicationService.findAll();

                int targetDrone = ThreadLocalRandom.current().nextInt(0, availableDrones.size());
                Long targetDroneId = availableDrones.get(targetDrone).id();
                int targetMedicationsAmount = ThreadLocalRandom.current().nextInt(0, medications.size());

                List<String> targetMedications = new ArrayList<>();
                for (int i = 0; i < targetMedicationsAmount; i++) {
                    int targetMedication = ThreadLocalRandom.current().nextInt(0, medications.size());

                    targetMedications.add(medications.stream()
                            .skip(targetMedication)
                            .map(Medication::getName)
                            .findFirst()
                            .orElse("RandomMedication"));
                }

                if (!targetMedications.isEmpty()) {
                    droneService.loadDrone(targetDroneId, new DroneLoadRequest(targetMedications));
                    droneService.setDroneBattery(targetDroneId, -10);
                    log.info("Drone #{} successfully loaded with medications: {}", targetDroneId, Arrays.toString(targetMedications.toArray()));
                }
            } catch (DroneIsBusyException e) {
                log.info("Current drone is busy at the moment");
            } catch (DroneBatteryLevelIsLowException e) {
                log.info("Current drone battery level is too low at the moment");
            } catch (DroneOverweightBadRequestException e) {
                log.info("Current drone can't carry that much medication");
            }
        } else {
            log.info("No available drones for medication delivery");
        }
    }

    @Scheduled(fixedDelay = 8000)
    private void scheduleDroneDelivering() {
        DronesAvailableResponse response = droneService.listOfDronesByState(DroneState.LOADED).getBody();
        List<DroneIdWithType> loadedDronesRecord = checkAvailableDrones(response);

        response = droneService.listOfDronesByState(DroneState.DELIVERING).getBody();
        List<DroneIdWithType> deliveringDronesRecord = checkAvailableDrones(response);

        List<DroneIdWithType> totalDronesRecord = Stream.concat(loadedDronesRecord.stream(), deliveringDronesRecord.stream()).toList();

        List<Drone> loadedDronesList = new ArrayList<>();
        makeDroneList(loadedDronesList, totalDronesRecord);

        for (Drone drone : loadedDronesList) {
            Long droneId = drone.getId();
            if (drone.getBatteryCapacity() >= 10) {
                log.info("Drone#{} delivering medication. Current battery level - {}%", droneId, drone.getBatteryCapacity());
                droneService.setDroneBattery(droneId, -10);
                drone.setBatteryCapacity(drone.getBatteryCapacity() - 10);
            }

            if (!drone.getDroneState().equals(DroneState.DELIVERING.name())) {
                droneService.updateDroneState(drone, DroneState.DELIVERING);
            }

            if (drone.getBatteryCapacity() == 0) {
                drone.setMedication("");
                droneService.updateDroneState(drone, DroneState.DELIVERED);
                droneService.loadDrone(droneId, new DroneLoadRequest(Collections.emptyList()));
            }
        }
    }

    @Scheduled(fixedDelay = 10000)
    private void scheduleDroneReturningRecovery() {
        DronesAvailableResponse response = droneService.listOfDronesByState(DroneState.DELIVERED).getBody();
        List<DroneIdWithType> deliveredDronesRecord = checkAvailableDrones(response);

        List<Drone> deliveredDronesList = new ArrayList<>();
        makeDroneList(deliveredDronesList, deliveredDronesRecord);

        for (Drone drone : deliveredDronesList) {
            Long droneId = drone.getId();
            log.info("Drone#{} finish delivering medication. Current battery level - {}%", droneId, drone.getBatteryCapacity());
            droneService.updateDroneState(drone, DroneState.RETURNING);
        }
    }

    @Scheduled(fixedDelay = 6000)
    private void scheduleDroneBatteryRecovery() {
        DronesAvailableResponse response = droneService.listOfDronesByState(DroneState.RETURNING).getBody();
        List<DroneIdWithType> returningDronesRecord = checkAvailableDrones(response);
        List<Drone> returnedDronesList = new ArrayList<>();
        makeDroneList(returnedDronesList, returningDronesRecord);

        for (Drone drone : returnedDronesList) {
            if (drone.getBatteryCapacity() != 100) {
                Long droneId = drone.getId();
                log.info("Drone#{} recharging. Current battery level - {}%", droneId, drone.getBatteryCapacity());
                droneService.setDroneBattery(droneId, 25);
                drone.setBatteryCapacity(drone.getBatteryCapacity() + 25);
            } else {
                droneService.updateDroneState(drone, DroneState.IDLE);
            }
        }
    }

    private List<DroneIdWithType> checkAvailableDrones(DronesAvailableResponse response) {
        return response == null ? new ArrayList<>() : response.getDroneList();
    }

    private void makeDroneList(List<Drone> dronesList, List<DroneIdWithType> listOfDroneRecords) {
        listOfDroneRecords.forEach(droneIdWithType -> {
            DroneInfoResponse droneInfoResponse = droneService.getDroneInfo(droneIdWithType.id()).getBody();
            dronesList.add(droneInfoResponse == null ? new Drone() : droneInfoResponse.getDrone());
        });
    }
}

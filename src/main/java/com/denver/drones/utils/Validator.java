package com.denver.drones.utils;

import com.denver.drones.entity.Medication;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@UtilityClass
public class Validator {

    private final String medicationNamePattern = "[a-zA-Z0-9-_]*";
    private final String medicationCodePattern = "[A-Z0-9_]*";
    private final String medicationImagePattern = "(.*?)\\.(jpg|gif|png)$";

    public boolean isMedicationDataWrong(Medication medication) {
        log.debug("Medication validation. Medication: {}", medication);

        return !medication.getName().matches(medicationNamePattern) ||
                !medication.getCode().matches(medicationCodePattern) ||
                !medication.getImage().matches(medicationImagePattern);
    }
}

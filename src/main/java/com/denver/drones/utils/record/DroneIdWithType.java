package com.denver.drones.utils.record;

public record DroneIdWithType(Long id, String type) {
}

package com.denver.drones.utils.record;

import org.springframework.http.HttpStatus;

public record ApiError(HttpStatus status, String message) {

}

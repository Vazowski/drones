package com.denver.drones.utils.record;

public record DroneIdWithBattery(Long id, int battery) {
}

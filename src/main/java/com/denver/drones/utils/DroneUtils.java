package com.denver.drones.utils;

import com.denver.drones.entity.Drone;
import com.denver.drones.entity.Medication;
import com.denver.drones.exception.drone.DroneOverweightBadRequestException;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Slf4j
@UtilityClass
public class DroneUtils {

    public List<String> checkDroneCanCarryMedications(Drone drone, List<Medication> medicationsFromDB, List<String> requestMedication) {
        int droneWeightLimit = drone.getWeightLimit();
        List<String> droneMedications = drone.getMedication() == null
                ? new ArrayList<>()
                : Arrays.asList(drone.getMedication().split(","));

        int droneCurrentWeight = medicationsFromDB.stream()
                .filter(medication -> droneMedications.contains(medication.getName()))
                .map(Medication::getWeight)
                .reduce(0, Integer::sum);

        int droneTargetWeight = medicationsFromDB.stream()
                .filter(medication -> requestMedication.contains(medication.getName()))
                .map(Medication::getWeight)
                .reduce(0, Integer::sum);

        log.debug("Drone check how many drugs can carry. DroneWeightLimit: {}, DroneCurrentWeight: {}, DroneTargetWeight: {}",
                droneWeightLimit, droneCurrentWeight, droneTargetWeight);

        if (droneCurrentWeight + droneTargetWeight > droneWeightLimit) {
            throw new DroneOverweightBadRequestException(HttpStatus.BAD_REQUEST);
        }

        if (!droneMedications.isEmpty()) {
            requestMedication.addAll(droneMedications);
        }
        return requestMedication;
    }
}

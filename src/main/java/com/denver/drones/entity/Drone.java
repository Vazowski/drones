package com.denver.drones.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "drone")
@NoArgsConstructor
public class Drone {

    @Id
    @Column(name = "id", updatable = false)
    @SequenceGenerator(
            name = "drone_sequence",
            sequenceName = "drone_sequence",
            allocationSize = 1,
            initialValue = 7
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "drone_sequence")
    private Long id;

    @NotBlank(message = "Serial number is mandatory")
    private String serialNumber;

    @NotBlank(message = "Drone model is mandatory")
    @Column(name = "model")
    private String droneModel;

    @Max(500)
    @NotNull
    private int weightLimit;

    @Max(100)
    @NotNull
    private int batteryCapacity;

    private String medication;

    @NotBlank(message = "Drone state is mandatory")
    @Column(name = "state")
    private String droneState;

    public Drone(String serialNumber, String droneModel) {
        this.serialNumber = serialNumber;
        this.droneModel = droneModel;
        this.droneState = "IDLE";
        this.weightLimit = 500;
        this.batteryCapacity = 100;
    }

    public Long getId() {
        return id;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getDroneModel() {
        return droneModel;
    }

    public void setDroneModel(String droneModel) {
        this.droneModel = droneModel;
    }

    public int getWeightLimit() {
        return weightLimit;
    }

    public void setWeightLimit(int weightLimit) {
        this.weightLimit = weightLimit;
    }

    public int getBatteryCapacity() {
        return batteryCapacity;
    }

    public void setBatteryCapacity(int batteryCapacity) {
        this.batteryCapacity = batteryCapacity;
    }

    public String getDroneState() {
        return droneState;
    }

    public void setDroneState(String droneState) {
        this.droneState = droneState;
    }

    public String getMedication() {
        return medication;
    }

    public void setMedication(String medication) {
        this.medication = medication;
    }

    @Override
    public String toString() {
        return "Drone{" +
                "id=" + id +
                ", serialNumber='" + serialNumber + '\'' +
                ", droneModel='" + droneModel + '\'' +
                ", weightLimit=" + weightLimit +
                ", batteryCapacity=" + batteryCapacity +
                ", medication='" + medication + '\'' +
                ", droneState='" + droneState + '\'' +
                '}';
    }
}

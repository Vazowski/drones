package com.denver.drones.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.List;

@Getter
@Setter
@EnableScheduling
@ConfigurationPropertiesScan
@ConfigurationProperties(prefix = "drones")
public class DronesConfiguration {

    private Long maxDronesAmount;
    private List<String> droneTypes;
    private List<String> droneState;
}

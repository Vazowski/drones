package com.denver.drones.service.impl;

import com.denver.drones.entity.Medication;
import com.denver.drones.exception.medication.MedicationBadRequestException;
import com.denver.drones.exception.medication.MedicationNotFoundException;
import com.denver.drones.model.medication.request.MedicationCreateRequest;
import com.denver.drones.model.medication.response.MedicationDeleteResponse;
import com.denver.drones.model.medication.response.MedicationInfoResponse;
import com.denver.drones.repository.MedicationRepository;
import com.denver.drones.service.MedicationService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class MedicationServiceImplTest {

    private static final String TEST_MEDICATION_NAME = "TEST_MEDICATION_NAME";
    private static final int TEST_MEDICATION_WEIGHT = 100;
    private static final String TEST_MEDICATION_CODE = "TEST_MEDICATION_CODE";
    private static final String TEST_MEDICATION_IMAGE = "TEST_MEDICATION_IMAGE.png";

    @Autowired
    private MedicationRepository medicationRepository;

    private MedicationService medicationService;

    MedicationCreateRequest request = new MedicationCreateRequest(TEST_MEDICATION_NAME, TEST_MEDICATION_WEIGHT, TEST_MEDICATION_CODE, TEST_MEDICATION_IMAGE);
    private static int currentMedicationIndex = 12;

    @BeforeEach
    void setUp() {
        medicationService = new MedicationServiceImpl(medicationRepository);
        medicationRepository.deleteAll();
    }


    @Test
    void whenMedicationReceived_thenSuccessfullyCreateInDB() {
        medicationService.createMedication(request);
        currentMedicationIndex++;
        Optional<Medication> medication = medicationRepository.findById((long) currentMedicationIndex);

        Assertions.assertAll(
                "Grouped Assertions for creating new Medication",
                () -> assertTrue(medication.isPresent()),
                () -> assertEquals(TEST_MEDICATION_NAME, medication.get().getName())
        );
    }

    @Test
    void whenWrongMedicationReceived_thenThrowException() {
        request = new MedicationCreateRequest(TEST_MEDICATION_NAME, TEST_MEDICATION_WEIGHT, "wrong_code", TEST_MEDICATION_IMAGE);
        MedicationBadRequestException exception = assertThrows(MedicationBadRequestException.class, () -> medicationService.createMedication(request));

        Assertions.assertAll(
                "Grouped Assertions for creating new Medication with wrong request",
                () -> assertEquals(HttpStatus.BAD_REQUEST, exception.getStatus())
        );
    }

    @Test
    void whenMedicationReceived_thenSuccessfullyUpdateInDB() {
        medicationService.createMedication(request);
        currentMedicationIndex++;
        request = new MedicationCreateRequest("NEW_TEST_MEDICATION", TEST_MEDICATION_WEIGHT, TEST_MEDICATION_CODE, TEST_MEDICATION_IMAGE);
        medicationService.updateMedication((long) currentMedicationIndex, request);

        Optional<Medication> medication = medicationRepository.findById((long) currentMedicationIndex);

        Assertions.assertAll(
                "Grouped Assertions for updating new Medication",
                () -> assertTrue(medication.isPresent()),
                () -> assertEquals("NEW_TEST_MEDICATION", medication.get().getName())
        );
    }

    @Test
    void whenNotExistingMedicationReceivedOnUpdate_thenThrowException() {
        MedicationNotFoundException exception = assertThrows(MedicationNotFoundException.class, () -> medicationService.updateMedication(0L, request));

        Assertions.assertAll(
                "Grouped Assertions for updating Medication with wrong data",
                () -> assertEquals(HttpStatus.BAD_REQUEST, exception.getStatus())
        );
    }

    @Test
    void whenWrongMedicationReceivedOnUpdate_thenThrowException() {
        medicationService.createMedication(request);
        currentMedicationIndex++;
        request = new MedicationCreateRequest(TEST_MEDICATION_NAME, TEST_MEDICATION_WEIGHT, "wrong_code", TEST_MEDICATION_IMAGE);
        MedicationBadRequestException exception = assertThrows(MedicationBadRequestException.class, () -> medicationService.updateMedication((long) currentMedicationIndex, request));

        Assertions.assertAll(
                "Grouped Assertions for updating wrong Medication",
                () -> assertEquals(HttpStatus.BAD_REQUEST, exception.getStatus())
        );
    }

    @Test
    void whenExistingIdReceived_thenReturnMedication() {
        medicationService.createMedication(request);
        currentMedicationIndex++;
        ResponseEntity<MedicationInfoResponse> response = medicationService.getMedicationInfo((long) currentMedicationIndex);

        Assertions.assertAll(
                "Grouped Assertions for getting Medication",
                () -> assertNotNull(response),
                () -> assertEquals(HttpStatus.OK, response.getStatusCode())
        );
    }

    @Test
    void whenWrongIdReceived_thenThrowException() {
        MedicationNotFoundException exception = assertThrows(MedicationNotFoundException.class, () -> medicationService.getMedicationInfo(0L));

        Assertions.assertAll(
                "Grouped Assertions for getting wrong Medication",
                () -> assertEquals(HttpStatus.BAD_REQUEST, exception.getStatus())
        );
    }

    @Test
    void whenExistingIdReceived_thenSuccessfullyDeleteMedication() {
        medicationService.createMedication(request);
        currentMedicationIndex++;
        ResponseEntity<MedicationDeleteResponse> response = medicationService.deleteMedication((long) currentMedicationIndex);

        Assertions.assertAll(
                "Grouped Assertions for deleting Medication",
                () -> assertNotNull(response),
                () -> assertEquals(0, medicationRepository.count())
        );
    }

    @Test
    void whenWrongIdReceivedOnDeleting_thenThrowException() {
        MedicationNotFoundException exception = assertThrows(MedicationNotFoundException.class, () -> medicationService.deleteMedication(0L));

        Assertions.assertAll(
                "Grouped Assertions for deleting wrong Medication",
                () -> assertEquals(HttpStatus.BAD_REQUEST, exception.getStatus())
        );
    }
}
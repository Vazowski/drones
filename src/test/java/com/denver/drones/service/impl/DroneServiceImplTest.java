package com.denver.drones.service.impl;

import com.denver.drones.config.DronesConfiguration;
import com.denver.drones.constants.DroneState;
import com.denver.drones.entity.Drone;
import com.denver.drones.entity.Medication;
import com.denver.drones.exception.drone.DroneBadRequestException;
import com.denver.drones.exception.drone.DroneIsBusyException;
import com.denver.drones.exception.drone.DroneMaxAmountException;
import com.denver.drones.exception.drone.DroneNotFoundException;
import com.denver.drones.model.drone.reponse.DroneBatteryResponse;
import com.denver.drones.model.drone.reponse.DroneDeleteResponse;
import com.denver.drones.model.drone.reponse.DroneInfoResponse;
import com.denver.drones.model.drone.reponse.DroneLoadResponse;
import com.denver.drones.model.drone.reponse.DronesAvailableResponse;
import com.denver.drones.model.drone.request.DroneCreateRequest;
import com.denver.drones.model.drone.request.DroneLoadRequest;
import com.denver.drones.repository.DroneRepository;
import com.denver.drones.service.DroneService;
import com.denver.drones.service.MedicationService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@SpringBootTest
class DroneServiceImplTest {

    private static final String TEST_DRONE_SERIAL_NUMBER = "TEST_SERIAL_NUMBER";
    private static final String TEST_MEDICATION_NAME = "TEST_MEDICATION_NAME";
    private static final int TEST_MEDICATION_WEIGHT = 100;
    private static final String TEST_MEDICATION_CODE = "TEST_MEDICATION_CODE";
    private static final String TEST_MEDICATION_IMAGE = "TEST_MEDICATION_IMAGE.png";

    @Autowired
    private DroneRepository droneRepository;
    @Autowired
    private DronesConfiguration dronesConfiguration;

    private static DroneService droneService;

    @Mock
    private MedicationService medicationService;

    private DroneCreateRequest request = new DroneCreateRequest(TEST_DRONE_SERIAL_NUMBER, "Lightweight");

    private static int currentDroneIndex = 6;

    @BeforeEach
    void setUp() {
        droneService = new DroneServiceImpl(droneRepository, medicationService, dronesConfiguration);

        droneRepository.deleteAll();
        while (droneRepository.count() < dronesConfiguration.getMaxDronesAmount()) {
            droneService.createDrone(request);
            currentDroneIndex++;
        }
    }

    @Test
    void whenBadDataReceived_thenThrowException() {
        request = new DroneCreateRequest(null, null);
        long initialDbRecords = droneRepository.count();

        DroneBadRequestException exception = assertThrows(DroneBadRequestException.class, () -> droneService.createDrone(request));

        long currentDbRecords = droneRepository.count();

        Assertions.assertAll(
                "Grouped Assertions for creating new Drone with empty request",
                () -> assertEquals(initialDbRecords, currentDbRecords),
                () -> assertEquals(HttpStatus.BAD_REQUEST, exception.getStatus())
        );
    }

    @Test
    void whenNewDroneReceivedOverMaxLimit_thenThrowException() {
        DroneMaxAmountException exception = assertThrows(DroneMaxAmountException.class, () -> droneService.createDrone(request));
        long currentDbRecords = droneRepository.count();

        Assertions.assertAll(
                "Grouped Assertions for creating new Drone over max limit",
                () -> assertEquals(dronesConfiguration.getMaxDronesAmount(), currentDbRecords),
                () -> assertEquals(HttpStatus.BAD_REQUEST, exception.getStatus())
        );
    }

    @Test
    void whenCorrectDroneReceived_thenSuccessfulCreateNewDroneInDb() {
        droneRepository.deleteAll();
        droneService.createDrone(request);
        currentDroneIndex++;

        Optional<Drone> drone = droneRepository.findById((long) currentDroneIndex);

        Assertions.assertAll(
                "Grouped Assertions for creating new Drone",
                () -> assertTrue(drone.isPresent()),
                () -> assertEquals(TEST_DRONE_SERIAL_NUMBER, drone.get().getSerialNumber())
        );
    }

    @Test
    void whenReceivedExistingId_thenReturnDrone() {
        ResponseEntity<DroneInfoResponse> response = droneService.getDroneInfo((long) currentDroneIndex);
        Assertions.assertAll(
                "Grouped Assertions for reading Drone",
                () -> assertNotNull(response),
                () -> assertSame(HttpStatus.OK, response.getStatusCode()),
                () -> assertEquals(TEST_DRONE_SERIAL_NUMBER, Objects.requireNonNull(response.getBody()).getDrone().getSerialNumber())
        );
    }

    @Test
    void whenReceivedNonExistingId_thenThrowException() {
        DroneNotFoundException exception = assertThrows(DroneNotFoundException.class, () -> droneService.getDroneInfo(0L));

        Assertions.assertAll(
                "Grouped Assertions for reading non-existing Drone",
                () -> assertEquals(HttpStatus.BAD_REQUEST, exception.getStatus())
        );
    }

    @Test
    void whenReceivedExistingId_thenDeleteDrone() {
        long initialDbRecords = droneRepository.count();
        ResponseEntity<DroneDeleteResponse> response = droneService.deleteDrone((long) currentDroneIndex);
        long currentDbRecords = droneRepository.count();
        Assertions.assertAll(
                "Grouped Assertions for deleting Drone",
                () -> assertNotNull(response),
                () -> assertSame(HttpStatus.OK, response.getStatusCode()),
                () -> assertEquals(initialDbRecords - 1, currentDbRecords)
        );
    }

    @Test
    void whenReceivedNonExistingId_thenDeleteDroneReturnException() {
        Long id = 0L;
        long initialDbRecords = droneRepository.count();
        DroneNotFoundException exception = assertThrows(DroneNotFoundException.class, () -> droneService.deleteDrone(id));

        long currentDbRecords = droneRepository.count();
        Assertions.assertAll(
                "Grouped Assertions for deleting non-existing Drone",
                () -> assertEquals(HttpStatus.BAD_REQUEST, exception.getStatus()),
                () -> assertEquals(initialDbRecords, currentDbRecords)
        );
    }

    @Test
    void whenReceivedWrongDrone_thenUnsuccessfullyLoadDroneAndThrowException() {
        DroneLoadRequest droneLoadRequest = new DroneLoadRequest(List.of(TEST_MEDICATION_NAME));
        DroneNotFoundException exception = assertThrows(DroneNotFoundException.class, () -> droneService.loadDrone(0L, droneLoadRequest));

        Assertions.assertAll(
                "Grouped Assertions loading wrong Drone",
                () -> assertEquals(HttpStatus.BAD_REQUEST, exception.getStatus())
        );
    }

    @Test
    void whenReceivedBusyDrone_thenThrowException() {
        DroneLoadRequest droneLoadRequest = new DroneLoadRequest(List.of(TEST_MEDICATION_NAME));
        droneService.loadDrone((long) currentDroneIndex, droneLoadRequest);

        DroneIsBusyException exception = assertThrows(DroneIsBusyException.class, () -> droneService.loadDrone((long) currentDroneIndex, droneLoadRequest));

        Assertions.assertAll(
                "Grouped Assertions loading busy Drone",
                () -> assertEquals(HttpStatus.BAD_REQUEST, exception.getStatus())
        );
    }

//    @Test
//    void whenReceivedDischargedDrone_thenThrowException() {
//        DroneLoadRequest droneLoadRequest = new DroneLoadRequest(List.of(TEST_MEDICATION_NAME));
//
//        if (droneRepository.count() < dronesConfiguration.getMaxDronesAmount()) {
//            droneService.createDrone(request);
//        }
//        droneService.loadDrone(currentDroneIndex, droneLoadRequest);
//
//        DroneBatteryLevelIsLowException exception = assertThrows(DroneBatteryLevelIsLowException.class, () -> droneService.loadDrone(currentDroneIndex, droneLoadRequest));
//
//        Assertions.assertAll(
//                "Grouped Assertions loading busy Drone",
//                () -> assertEquals(HttpStatus.BAD_REQUEST, exception.getStatus())
//        );
//    }

    @Test
    void whenReceivedCorrectDroneWithOneMedication_thenSuccessfullyLoadDrone() {
        Medication medication = new Medication();
        medication.setName(TEST_MEDICATION_NAME);
        medication.setWeight(TEST_MEDICATION_WEIGHT);
        medication.setCode(TEST_MEDICATION_CODE);
        medication.setImage(TEST_MEDICATION_IMAGE);

        DroneLoadRequest droneLoadRequest = new DroneLoadRequest(List.of(TEST_MEDICATION_NAME));

        when(medicationService.findAll()).thenReturn(List.of(medication));

        ResponseEntity<DroneLoadResponse> droneLoadResponse = droneService.loadDrone((long) (currentDroneIndex), droneLoadRequest);

        ResponseEntity<DroneInfoResponse> droneInfoResponse = droneService.getDroneInfo((long) currentDroneIndex);

        Assertions.assertAll(
                "Grouped Assertions for loading Drone with one medication",
                () -> assertNotNull(droneLoadResponse),
                () -> assertNotNull(droneInfoResponse),
                () -> assertSame(HttpStatus.OK, droneLoadResponse.getStatusCode()),
                () -> assertSame(HttpStatus.OK, droneInfoResponse.getStatusCode()),
                () -> assertIterableEquals(List.of(TEST_MEDICATION_NAME), List.of(droneInfoResponse.getBody().getDrone().getMedication()))
        );
    }

    @Test
    void whenReceivedCorrectDroneWithThreeMedication_thenSuccessfullyLoadDrone() {
        String testMedicationName2 = TEST_MEDICATION_NAME + "2";
        String testMedicationName3 = TEST_MEDICATION_NAME + "3";
        Medication medication1 = new Medication();
        medication1.setName(TEST_MEDICATION_NAME + "1");
        medication1.setWeight(TEST_MEDICATION_WEIGHT);
        medication1.setCode(TEST_MEDICATION_CODE + "1");
        medication1.setImage(TEST_MEDICATION_IMAGE);

        Medication medication2 = new Medication();
        medication2.setName(testMedicationName2);
        medication2.setWeight(TEST_MEDICATION_WEIGHT + 50);
        medication2.setCode(TEST_MEDICATION_CODE + "2");
        medication2.setImage(TEST_MEDICATION_IMAGE);

        Medication medication3 = new Medication();
        medication3.setName(testMedicationName3);
        medication3.setWeight(TEST_MEDICATION_WEIGHT + 150);
        medication3.setCode(TEST_MEDICATION_CODE + "3");
        medication3.setImage(TEST_MEDICATION_IMAGE);

        DroneLoadRequest droneLoadRequest = new DroneLoadRequest(List.of(TEST_MEDICATION_NAME, testMedicationName2, testMedicationName3));

        when(medicationService.findAll()).thenReturn(List.of(medication1, medication2, medication3));

        ResponseEntity<DroneLoadResponse> droneLoadResponse = droneService.loadDrone((long) currentDroneIndex, droneLoadRequest);

        ResponseEntity<DroneInfoResponse> droneInfoResponse = droneService.getDroneInfo((long) currentDroneIndex);

        Assertions.assertAll(
                "Grouped Assertions for loading Drone with three medications",
                () -> assertNotNull(droneLoadResponse),
                () -> assertNotNull(droneInfoResponse),
                () -> assertSame(HttpStatus.OK, droneLoadResponse.getStatusCode()),
                () -> assertSame(HttpStatus.OK, droneInfoResponse.getStatusCode()),
                () -> assertIterableEquals(List.of(TEST_MEDICATION_NAME, testMedicationName2, testMedicationName3), List.of(droneInfoResponse.getBody().getDrone().getMedication().split(", ")))
        );
    }

    @Test
    void loadListOfAvailableDrones() {
        ResponseEntity<DronesAvailableResponse> response = droneService.listOfDronesByState(DroneState.IDLE);

        Assertions.assertAll(
                "Grouped Assertions for receiving Drone list",
                () -> assertSame(HttpStatus.OK, response.getStatusCode()),
                () -> assertEquals(3, response.getBody().getDroneList().size())
        );
    }

    @Test
    void whenReceivedCorrectDrone_therReturnBatteryInfo() {
        ResponseEntity<DroneBatteryResponse> response = droneService.droneWithBatteryInfo((long) currentDroneIndex);

        Assertions.assertAll(
                "Grouped Assertions for receiving fully charged Drone by id",
                () -> assertSame(HttpStatus.OK, response.getStatusCode()),
                () -> assertEquals(100, response.getBody().getDrone().battery())
        );
    }

    @Test
    void whenReceivedWrongDroneForBatteryInfo_thenReturnException() {
        droneRepository.deleteAll();
        DroneNotFoundException exception = assertThrows(DroneNotFoundException.class, () -> droneService.droneWithBatteryInfo(0L));

        Assertions.assertAll(
                "Grouped Assertions for receiving fully charged Drone by wrong id",
                () -> assertEquals(HttpStatus.BAD_REQUEST, exception.getStatus())
        );
    }

    @Test
    void changeDroneBattery() {
        droneRepository.deleteAll();
        droneService.createDrone(request);
        currentDroneIndex++;

        droneService.setDroneBattery((long) currentDroneIndex, -101);

        ResponseEntity<DroneInfoResponse> response = droneService.getDroneInfo((long) currentDroneIndex);
        Assertions.assertAll(
                "Grouped Assertions for receiving Drone battery info",
                () -> assertSame(HttpStatus.OK, response.getStatusCode()),
                () -> assertEquals(0, response.getBody().getDrone().getBatteryCapacity())
        );
    }
}
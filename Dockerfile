FROM openjdk:17-jdk
COPY ./target/drones-1.0.0-SNAPSHOT.jar /temp/drones-1.0.0-SNAPSHOT.jar
WORKDIR /temp
EXPOSE 8010
ENTRYPOINT ["java", "-jar", "drones-1.0.0-SNAPSHOT.jar"]
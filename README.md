# Drones
There is a major new technology that is destined to be a disruptive force in the field of transportation: **the drone**. Just as the mobile phone allowed developing countries to leapfrog older technologies for personal communication, the drone has the potential to leapfrog traditional transportation infrastructure.

Useful drone functions include delivery of small items that are (urgently) needed in locations with difficult access.

The future is already here! Whether we like it or not, new technologies are gradually entering our daily lives. Could you have imagined that very soon, sitting on a couch and holding a mobile phone or any other gadget, you will be able to control a flying drone? So, imagine now that you have an army of flying drones at your fingertips. They never get tired, they don't say anything, they don't even need to be fed, and they never have a headache.

All you need to do is to take your favorite gadget and say what you want and when you want it. And your faithful assistant will do all your work for you. Meanwhile, you can pet your cat.

## Description
Drones solve the problem of delivering goods. The goods are not limited to any specific type, and their list expands rapidly. Drones carry out delivery automatically; all you need to do is explain where and how to deliver and set up the loading/unloading process.

## Getting Started

### Built with
The project is built using well-known technologies such as:
* Java 17
* SpringBoot 3
* Docker
* Docker-compose or PostgreSQL
* H2 for test
* Liquibase
* Swagger
* Maven

### Installing
The installation process looks as follows:

a) using your DB, your need to set datasource in *application.yml* like DB url, username and user password

b) using docker your need to set environment in *docker-compose.yml* like DB username, user password and port 

### Building program

```mvn clean install```

```mvn test```

### Running the program
```docker-compose up```

```java -jar drones-1.0.0.jar```

## Usage
The graphical interface is currently under development. The application is working in console mode. 

You can also use Swagger at the following address: http://localhost:8080/swagger-ui/index.html

## Contact
Denis Ermakov - vazovski1987@gmail.com 

## Version History
* 1.0.0
  * Initial Release